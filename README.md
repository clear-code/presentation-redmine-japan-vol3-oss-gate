# 今日から参加できる！OSS開発

[Redmine Japan - Redmine Japan Vol.3](https://redmine-japan.org/)での発表資料です。

## ライセンス

CC BY-SA 4.0

詳細は[LICENSE](./LICENSE)を確認してください。

## スライド

### 表示

```console
% rake
```

### 公開

```console
% rake publish
```
